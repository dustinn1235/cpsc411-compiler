#ifndef SCANNER_H
#define SCANNER_H
#include <stdio.h>
#include <string>

// Reserved words: true, false, boolean, int, void, if, else, while, break, return.
// +, -, *, /, %, <, >, <=, >=, =, ==, !=, !, &&, ||.
// Other things: parentheses (), braces {}, semicolon ;, comma ,.

enum Tokens {
    T_ID = 1,
    T_TRUE,
    T_FALSE,
    T_BOOLEAN,
    T_INT,
    T_VOID,
    T_IF,
    T_ELSE,
    T_WHILE,
    T_BREAK,
    T_RETURN,
    T_NUM,
    T_ADD,
    T_SUB,
    T_MULT,
    T_DIV,
    T_MOD,
    T_LT,
    T_GT,
    T_LE,
    T_GE,
    T_ASGN,
    T_EQ,
    T_NEQ,
    T_NOT,
    T_AND,
    T_OR,
    T_LP,
    T_RP,
    T_LB,
    T_RB,
    T_SEMI,
    T_COMMA,
    T_STRING
};

extern int yylex();
extern FILE *yyin;
extern int yylineno;
extern char* yytext;
extern int yyleng;
extern std::string word;

#endif