%option noyywrap
%option yylineno

/* Declaration */
%{
    #include "scanner.h"
    int errNo = 0;
    std::string word = "";
%}

/* Definition */
alpha   [a-zA-Z]
digit   [0-9]
alnum   [a-zA-Z0-9_]

num     {digit}{digit}*
ID      {alpha}{alnum}*|_{alnum}*

/* Comment States */
%x COMMENT
%x STRING

%%
\n;
[[:space:]]|[[:space:]]+;

">"             return T_GT;
"<"             return T_LT;
">="            return T_GE;
"<="            return T_LE;
"+"             return T_ADD;
"-"             return T_SUB;
"/"             return T_DIV;
"*"             return T_MULT;
"%"             return T_MOD;
"="             return T_ASGN;
"=="            return T_EQ;
"!="            return T_NEQ;
"!"             return T_NOT;
"&&"            return T_AND;
"||"            return T_OR;
"true"          return T_TRUE;
"false"         return T_FALSE;
"boolean"       return T_BOOLEAN;
"int"           return T_INT;
"void"          return T_VOID;
"if"            return T_IF;
"else"          return T_ELSE;
"while"         return T_WHILE;
"break"         return T_BREAK;
"return"        return T_RETURN;
"("             return T_LP;
")"             return T_RP;
"{"             return T_LB;
"}"             return T_RB;
";"             return T_SEMI;
","             return T_COMMA;

{ID}            return T_ID;
{num}           return T_NUM;

"//"            BEGIN(COMMENT);
<COMMENT>. 
<COMMENT>\n     {BEGIN(INITIAL);}

\"              {BEGIN(STRING);}
<STRING>\"      {BEGIN(INITIAL); return T_STRING;}
<STRING>\n      {fprintf(stderr, "Error: string missing closing quote at line %d\n", yylineno); return 0;} 
<STRING><<EOF>> {fprintf(stderr, "Error: string missing closing quote at line %d\n", yylineno); return 0;} 
<STRING>\\\"    {word.push_back(*yytext);}
<STRING>.       {word.push_back(*yytext);}

.               {fprintf(stderr, "Warning: illegal char (%s) at line %d\n", yytext, yylineno); errNo++; 
                if (errNo == 10) {fprintf(stderr, "Error: too many warnings at line %d\n", yylineno); return 0;}}

%%