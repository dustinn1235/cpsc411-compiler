#include "scanner.h"
#include <stdlib.h>
#include <errno.h>
#include <iostream>
using namespace std;

char const* getName(int tok){
    switch (tok)
    {
        case T_ID: return "ID";
        case T_TRUE: return "TRUE";
        case T_FALSE: return "FALSE";
        case T_BOOLEAN: return "BOOLEAN";
        case T_INT: return "INT";
        case T_VOID: return "VOID";
        case T_IF: return "IF";
        case T_ELSE: return "ELSE";
        case T_WHILE: return "WHILE";
        case T_BREAK: return "BREAK";
        case T_RETURN: return "RETURN";
        case T_NUM: return "Num";
        case T_ADD: return "+";
        case T_SUB: return "-";
        case T_DIV: return "/";
        case T_MULT: return "*";
        case T_MOD: return "%";
        case T_LT: return "<";
        case T_GT: return ">";
        case T_GE: return ">=";
        case T_LE: return "<=";
        case T_ASGN: return "=";
        case T_EQ: return "==";
        case T_NEQ: return "!=";
        case T_NOT: return "!";
        case T_AND: return "&&";
        case T_OR: return "||";
        case T_LP: return "(";
        case T_RP: return ")";
        case T_LB: return "{";
        case T_RB: return "}";
        case T_SEMI: return ";";
        case T_COMMA: return ",";
        case T_STRING: return "String";
    }
    return "ERR";
}

int main(int argc, char **argv) {
    if (argc == 2) {
        yyin = fopen(argv[1], "r");

        if (!yyin) {
            fprintf(stderr, "Failed to open file %s\n", argv[1]);
            perror("Error: ");
            return EXIT_FAILURE;
        }
    }
    int tok;
    // Get token until EOF
    while ((tok = yylex()) != 0){
        if (getName(tok) == "ID" || getName(tok) == "Num")
            fprintf(stdout, "Line: %d; Token: %s; Lexeme: %s\n", yylineno, getName(tok), yytext);
        else if (getName(tok) == "String"){
            cout << "Line: " << yylineno << "; Token: " << getName(tok) << "; Lexeme: " << word << endl;
        }
        else
            fprintf(stdout, "Line: %d; Token: %s;\n", yylineno, getName(tok));
    }

    return EXIT_SUCCESS;
}
